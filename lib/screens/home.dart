import 'package:flutter/material.dart';
import 'package:bestcycling_test/widgets/header.dart';
import 'package:bestcycling_test/widgets/scores/index.dart';
import 'package:bestcycling_test/widgets/lessons/index.dart';
import 'package:bestcycling_test/widgets/news/index.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen();
  static const String routeName = '/home';

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State <HomeScreen> {

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      width: width,
      height: height,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: 35, bottom: 12),
              color: Color(0xFF262626),
              child: Header()
            ),
            Container(
              color: Color(0xFF262626),
              child: ScoresWidget()
            ),
            SizedBox(height: 50,),
            LessonsWidget(),
            SizedBox(height: 30,),
            NewsWidget(),
          ],
        ),
      ),
    );
  }
}
