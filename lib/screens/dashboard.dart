import 'package:flutter/material.dart';

import './home.dart';
import './lessons.dart';

class Dashboard extends StatefulWidget {
  Dashboard();
  static const String routeName = '/';

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State <Dashboard> {
  PageController _pageController;
  var _page = 0;

  void navigationTapped(int page) {
    _pageController.animateToPage(
      page,
      duration: Duration(milliseconds: 300),
      curve: Curves.easeIn,
    );
  }

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }

  @override
  void initState() {
    super.initState();
    _pageController = new PageController();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF151515),
      body: PageView(
        children: [
          HomeScreen(),
          LessonsScreen(),
          Center(
            child: Text('Sensei', style: TextStyle(color: Colors.white)),
          ),
          Center(
            child: Text('Notificaciones', style: TextStyle(color: Colors.white)),
          ),
          Center(
            child: Text('Perfil', style: TextStyle(color: Colors.white)),
          ),
        ],
        controller: _pageController,
        onPageChanged: onPageChanged,
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Color(0xFF262626),
        items: [
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Image.asset('assets/images/home@3x.png', width: 20,),
            ),
            activeIcon: Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Image.asset('assets/images/home-selected@3x.png', width: 20,),
            ),
            label: 'Inicio',
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Image.asset('assets/images/search@3x.png', width: 20,),
            ),
            activeIcon: Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Image.asset('assets/images/search-selected@3x.png', width: 20,),
            ),
            label: 'Buscar',
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Image.asset('assets/images/sensei@3x.png', width: 28),
            ),
            activeIcon: Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Image.asset('assets/images/sensei-selected@3x.png', width: 28),
            ),
            label: 'Sensei',
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Image.asset('assets/images/notifications@3x.png', width: 20,),
            ),
            activeIcon: Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Image.asset('assets/images/notifications-selected@3x.png', width: 20,),
            ),
            label: 'Notificaciones',
          ),
          BottomNavigationBarItem(
            icon: Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Image.asset('assets/images/profile@3x.png', width: 20,),
            ),
            activeIcon: Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: Image.asset('assets/images/profile-selected@3x.png', width: 20,),
            ),
            label: 'Perfil',
          ),
        ],
        onTap: navigationTapped,
        currentIndex: _page,
        fixedColor: Colors.white,
        unselectedItemColor: Colors.grey,
        unselectedLabelStyle: TextStyle(
          fontSize: 9
        ),
        selectedLabelStyle: TextStyle(
          fontSize: 9
        ),
      ),
    );
  }
}
