import 'package:flutter/material.dart';

class LessonScreen extends StatefulWidget {
  final Map<String, dynamic> lesson;
  LessonScreen(this.lesson);

  static const String routeName = '/lesson';

  @override
  _LessonScreenState createState() => _LessonScreenState();
}

class _LessonScreenState extends State <LessonScreen> {
  

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Color(0xFF151515),
      appBar: AppBar(
        title: Text(widget.lesson['title']),
        centerTitle: true,
      ),
      body: Center(
        child: Text('$width x $height', style: TextStyle(color: Colors.white)),
      ),
    );
  }
}
