import 'package:bestcycling_test/repository/training_class_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:bestcycling_test/blocs/training_class_bloc.dart';
import 'package:bestcycling_test/blocs/training_class_event.dart';
import 'package:bestcycling_test/blocs/training_class_state.dart';
import 'package:bestcycling_test/widgets/lessons/lesson_list.dart';

class LessonsScreen extends StatefulWidget {
  LessonsScreen();

  static const String routeName = '/lesson';

  @override
  _LessonsScreenState createState() => _LessonsScreenState();
}

class _LessonsScreenState extends State <LessonsScreen> {
  TrainingClassBloc _bloc;

  @override
  void initState() {
    super.initState();

    _bloc = TrainingClassBloc(trainingClassRepository: TrainingClassRepository.instance);
    _bloc.add(GetTrainingClassEvet());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Elige un entrenamient'),
        centerTitle: true,
      ),
      backgroundColor: Color(0xFF151515),
      body: Center(
        child: BlocBuilder<TrainingClassBloc, BaseTrainingState>(
          bloc: _bloc,
          builder: (BuildContext context, BaseTrainingState state) {
            print('State $state');
            if (state is LoadingTrainingClass) {
              return CircularProgressIndicator();
            } else if (state is SuccesTrainingClass) {
              return ListView.builder(
                itemCount: TrainingClassRepository.instance.trainingClasses.length,
                itemBuilder: (context, index) {
                  var item = TrainingClassRepository.instance.trainingClasses[index];
                  print('item $item');
                  return LessonListWidget(item);
                }
              );
            } else if (state is ErrorTrainingClass) {
              return Text('Error garrafal');
            }

            return Text('Buscar');
          },
      )),
    );
  }
}
