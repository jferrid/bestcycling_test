abstract class BaseTrainingState {}

class LoadingTrainingClass extends BaseTrainingState {}
class SuccesTrainingClass extends BaseTrainingState {}
class ErrorTrainingClass extends BaseTrainingState {
  final String message;

  ErrorTrainingClass(this.message);
}

class InitialTrainingClass extends BaseTrainingState {}
