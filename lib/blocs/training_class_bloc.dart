import 'package:bestcycling_test/blocs/training_class_event.dart';
import 'package:bestcycling_test/repository/training_class_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'training_class_state.dart';

class TrainingClassBloc extends Bloc<TrainingClassEvent, BaseTrainingState> {
  final TrainingClassRepository trainingClassRepository;

  TrainingClassBloc({ @required this.trainingClassRepository });

  @override
  BaseTrainingState get initialState => InitialTrainingClass();

  @override
  Stream<BaseTrainingState> mapEventToState(TrainingClassEvent event) async* {
    if (event is GetTrainingClassEvet) {
      // get classes
      yield LoadingTrainingClass();
      try {
        await trainingClassRepository.getTrainingClasses();
        yield SuccesTrainingClass();
      } catch (error) {
        print('Error $error');
        yield ErrorTrainingClass(error.toString());
      }
    }
  }
}