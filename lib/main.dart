import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import './screens/dashboard.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return MaterialApp(
      title: 'Bestcycling Test',
      theme: ThemeData(
        primaryColor: Color(0xFF262626)
      ),
      routes: {
        Dashboard.routeName: (context) => Dashboard()
      },
    );
  }
}
