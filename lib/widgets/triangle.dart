import 'package:flutter/material.dart';

class CustomTriangleClipper extends CustomClipper<Path> {
  @override Path getClip(Size size) {
    final path = Path();
    path.moveTo(size.width, 0);
    path.lineTo(0, size.height);
    path.lineTo(size.height, size.width);
    path.close();
    return path;
  }
  @override bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}