import 'package:intl/intl.dart';

import 'package:bestcycling_test/models/training_class.dart';
import 'package:flutter/material.dart';
import 'lessons_widgets/level.dart';

class LessonListWidget extends StatelessWidget {
  final TrainingClass lesson;

  const LessonListWidget(this.lesson);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Column(
      children: [
        Container(
          width: width,
          height: 60,
          padding: EdgeInsets.only(top: 16, left: 16, right: 16),
          color: Colors.black,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Text(
                lesson.title,
                style: TextStyle(
                  color: Colors.grey[400],
                  fontWeight: FontWeight.bold,
                  fontSize: 14
                ),
              ),
              SizedBox(height: 7,),
              Text(
                'Teacher',
                style: TextStyle(color: Colors.grey[400], fontSize: 14),
              ),
            ],
          ),
        ),
        Container(
          width: width,
          height: 130,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: NetworkImage(lesson.cover),
              fit: BoxFit.cover
            )
          ),
          padding: EdgeInsets.all(10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
            Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 28,
                width: 28,
                alignment: Alignment.center,
                margin: EdgeInsets.only(right: 3),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xFFd19800)
                ),
                child: Text(
                  lesson.levelNr.toString(),
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              Container(
                height: 28,
                width: 28,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xFFf33841),
                ),
                child: Text(
                  lesson.trainingTypeNr.toString(),
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              )
            ],
          ),
          Image(image: NetworkImage(lesson.graph), width: 100)
          ],)
        ),
        Container(
          height: 60,
          padding: EdgeInsets.symmetric(horizontal: 16, vertical: 5),
          color: Color(0xFF262626),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 50,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    LevelWidget(lesson.levelNr),
                    SizedBox(height: 5,),
                    Text(lesson.category, style: TextStyle(color: Colors.white, fontSize: 14)),
                  ],
                ),
              ),
              Text(DateFormat('dd MMM').format(DateTime.parse(lesson.publishedAt)), style: TextStyle(color: Colors.grey, fontSize: 14),),
              RichText(
                text: TextSpan(
                  style: TextStyle(fontSize: 14, color: Colors.white),
                  children: <TextSpan> [
                    TextSpan(
                      text: 'Duración ',
                    ),
                    TextSpan(
                      text: "${((lesson.durationTraining / 60).truncate() % 60).toString().padLeft(2, '0')}'",
                      style: TextStyle(color: Colors.grey),
                    ),
                  ]
                )
              ),
            ],
          ),
        ),
      ],
    );
  }
}
