import 'package:bestcycling_test/screens/lesson.dart';
import 'package:flutter/material.dart';

import './lessons_widgets/info.dart';
import './lessons_widgets/bottom_category.dart';

class LessonWidget extends StatelessWidget {
  final Map<String, dynamic> lesson;

  const LessonWidget(this.lesson);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => LessonScreen(lesson))
        );
      },
      child: Container(
        width: width * 0.85,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Image.asset('assets/images/logo_bestcycling@3x.png', width: 100,),
                Text(lesson['date'], style: TextStyle(color: Color(0xFF7a7a7a)),)
              ],
            ),
            SizedBox(height: 5,),
            InfoWidget(lesson),
            Container(
              height: 100,
              color: Colors.black,
              child: BottomCategoryWidget(lesson)
            ),
          ],
        ),
      ),
    );
  }
}
