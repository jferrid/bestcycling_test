import 'package:flutter/material.dart';
import '../headerSection.dart';
import './list.dart';

class LessonsWidget extends StatelessWidget {
  LessonsWidget();

  final List<Map<String, dynamic>> _staticLessons = [
    {
      'date': '24, Diciembre',
      'title': 'BC12 / Diversión, la clave',
      'teacher': 'Dolo Bayarri González',
      'topic': 'Variado',
      'level': 2,
      'duration': "49'",
      'right': '12',
      'left': '3'
    },
    {
      'date': '24, Diciembre',
      'title': 'BC13 / Diversión, la clave',
      'teacher': 'Dolo Bayarri González',
      'topic': 'Variado',
      'level': 2,
      'duration': "49'",
      'right': '12',
      'left': '3'
    },
    {
      'date': '24, Diciembre',
      'title': 'BC14 / Diversión, la clave',
      'teacher': 'Dolo Bayarri González',
      'topic': 'Variado',
      'level': 2,
      'duration': "49'",
      'right': '12',
      'left': '3'
    },
    {
      'date': '24, Diciembre',
      'title': 'BC15 / Diversión, la clave',
      'teacher': 'Dolo Bayarri González',
      'topic': 'Variado',
      'level': 2,
      'duration': "49'",
      'right': '12',
      'left': '3'
    }
  ];

  @override
  Widget build(BuildContext context) =>
    Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        children: [
          HeaderSection(title: 'ÚLTIMAS CLASES AÑADIDAS'),
          SizedBox(height: 50,),
          LessonsListWidget(_staticLessons),
        ],
      ),
    );
}
