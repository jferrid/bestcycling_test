import 'package:flutter/material.dart';

import './level.dart';

class InfoWidget extends StatelessWidget {
  final Map<String, dynamic> lesson;
  const InfoWidget(this.lesson);

  @override
  Widget build(BuildContext context) =>
    Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(10),
      color: Color(0xFF262626),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            lesson['title'],
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 14
            ),
          ),
          SizedBox(height: 12,),
          Text(
            lesson['teacher'],
            style: TextStyle(color: Colors.grey, fontSize: 12),
          ),
          SizedBox(height: 12,),
          Text(
            lesson['topic'],
            style: TextStyle(color: Colors.grey, fontSize: 12),
          ),
          SizedBox(height: 7,),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              LevelWidget(lesson['level']),
              RichText(
                text: TextSpan(
                  style: TextStyle(fontSize: 12, color: Colors.white),
                  children: <TextSpan> [
                    TextSpan(
                      text: 'Duración ',
                    ),
                    TextSpan(
                      text: lesson['duration'],
                      style: TextStyle(color: Colors.grey),
                    ),
                  ]
                )
              ),
            ],
          ),
        ],
      )
    );
}
