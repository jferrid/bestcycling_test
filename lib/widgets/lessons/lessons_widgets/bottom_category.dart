import 'package:flutter/material.dart';

import '../../triangle.dart';

class BottomCategoryWidget extends StatelessWidget {
  final Map<String, dynamic> level;
  const BottomCategoryWidget(this.level);

  @override
  Widget build(BuildContext context) =>
    Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Container(
          height: 45,
          padding: EdgeInsets.only(left: 10),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 28,
                width: 28,
                alignment: Alignment.center,
                margin: EdgeInsets.only(right: 3),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xFFd19800)
                ),
                child: Text(
                  level['left'] ?? '0',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
              Container(
                height: 28,
                width: 28,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xFFf33841),
                ),
                child: Text(
                  level['right'] ?? '0',
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
              )
            ],
          ),
        ),
        ClipPath(
          clipper: CustomTriangleClipper(),
          child: Container(
            width: 30,
            height: 30,
            color: Colors.orange
          ),
        )
      ],
    );
}
