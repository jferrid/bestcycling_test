import 'package:flutter/material.dart';
import './lesson.dart';

class LessonsListWidget extends StatelessWidget {
  final List<Map<String, dynamic>> lessons;

  const LessonsListWidget(this.lessons);

  @override
  Widget build(BuildContext context) =>
    Container(
      height: 300,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: lessons.length,
        itemBuilder: (context, index) {
          Map<String, dynamic> lesson = lessons[index];
          return Container(
            margin: EdgeInsets.only(right: 16),
            child: LessonWidget(lesson),
          );
        }
      ),
    );
}
