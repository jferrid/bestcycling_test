import 'package:flutter/material.dart';

class Header extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Image.asset('assets/images/bestlife-logo@3x.png', width: 100,)
    );
  }
}
