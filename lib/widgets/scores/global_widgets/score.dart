import 'package:flutter/material.dart';

class GlobalScoreWidget extends StatelessWidget {
  final int score;
  final String title;

  const GlobalScoreWidget({
    @required this.score,
    @required this.title
  }); 

  @override
  Widget build(BuildContext context) =>
    Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          score.toString(),
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w500,
            fontSize: 18,
          ),
        ),
        Text(
          title,
          style: TextStyle(
            color: Colors.white,
          ),
        )
      ],
    );
}
