import 'package:flutter/material.dart';
import './global.dart';
import './specific.dart';

class ScoresWidget extends StatelessWidget {
  final int level, constancy, points, resistance, strength, flexibility, mind;

  const ScoresWidget({
    this.level = 0,
    this.constancy = 0,
    this.points = 0,
    this.resistance = 0,
    this.strength = 10,
    this.flexibility = 0,
    this.mind = 0
  }); 

  @override
  Widget build(BuildContext context) =>
    Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        children: [
          GlobalScoresWidget(level, constancy, points),
          SpecificScoresWidget(resistance, strength, flexibility, mind)
        ],
      ),
    );
}
