import 'package:flutter/material.dart';
import './specific_widget/score.dart';

class SpecificScoresWidget extends StatelessWidget {
  final int resistance, strength, flexibility, mind;

  const SpecificScoresWidget(
    this.resistance,
    this.strength,
    this.flexibility,
    this.mind
  ); 

  @override
  Widget build(BuildContext context) =>
    Container(
      padding: EdgeInsets.only(bottom: 20, top: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SpecificScoreWidget(score: resistance, title: 'Resistencia'),
          SpecificScoreWidget(score: strength, title: 'Fuerza'),
          SpecificScoreWidget(score: flexibility, title: 'Flexibilidad'),
          SpecificScoreWidget(score: mind, title: 'Mente')
        ],
      ),
    );
}
