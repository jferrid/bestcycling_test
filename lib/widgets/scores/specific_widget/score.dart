import 'package:flutter/material.dart';

class SpecificScoreWidget extends StatelessWidget {
  final int score;
  final String title;

  const SpecificScoreWidget({
    @required this.score,
    @required this.title
  }); 

  @override
  Widget build(BuildContext context) =>
    Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
          width: 71,
          height: 71,
          padding: EdgeInsets.only(top: 5),
          alignment: Alignment.center,
          margin: EdgeInsets.only(bottom: 5),
          decoration: BoxDecoration(
            color: Color(0xFF151515),
            shape: BoxShape.circle
          ),
          child: Text(
            score.toString(),
            style: TextStyle(
              color: Colors.white,
              fontSize: 28,
              fontWeight: FontWeight.bold
            ),
          ),
        ),
        Text(
          title,
          style: TextStyle(
            color: Colors.white
          ),
        ),
      ],
    );
}
