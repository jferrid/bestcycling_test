import 'package:flutter/material.dart';
import './global_widgets/score.dart';

class GlobalScoresWidget extends StatelessWidget {
  final int level, constancy, points;

  const GlobalScoresWidget(
    this.level,
    this.constancy,
    this.points
  ); 

  @override
  Widget build(BuildContext context) =>
    Container(
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
      decoration: BoxDecoration(
        border: Border(
          top: BorderSide(color: Color(0xFF343434), width: 2),
          bottom: BorderSide(color: Color(0xFF343434), width: 1)
        )
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GlobalScoreWidget(score: level, title: 'NIVEL'),
          GlobalScoreWidget(score: constancy, title: 'COSTANCIA'),
          GlobalScoreWidget(score: points, title: 'PUNTOS')
        ],
      ),
    );
}
