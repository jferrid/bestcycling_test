import 'package:flutter/material.dart';
import '../headerSection.dart';
import './list.dart';

class NewsWidget extends StatelessWidget {
  NewsWidget();

  final List<Map<String, dynamic>> _staticNews = [
    {
      'date': 'hace 23 horas',
      'resume': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis interdum est, sed consequat quam. Fusce vel maximus diam. Suspendisse molestie tincidunt sapien, sit amet ullamcorper est tempus id. Vivamus iaculis, eros vel accumsan congue, diam nisi elementum ex, non condimentum nibh quam vel velit.'
    },
    {
      'date': 'hace 23 horas',
      'resume': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis interdum est, sed consequat quam. Fusce vel maximus diam. Suspendisse molestie tincidunt sapien, sit amet ullamcorper est tempus id. Vivamus iaculis, eros vel accumsan congue, diam nisi elementum ex, non condimentum nibh quam vel velit.'
    },
    {
      'date': 'hace 23 horas',
      'resume': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis interdum est, sed consequat quam. Fusce vel maximus diam. Suspendisse molestie tincidunt sapien, sit amet ullamcorper est tempus id. Vivamus iaculis, eros vel accumsan congue, diam nisi elementum ex, non condimentum nibh quam vel velit.'
    },
    {
      'date': 'hace 23 horas',
      'resume': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas quis interdum est, sed consequat quam. Fusce vel maximus diam. Suspendisse molestie tincidunt sapien, sit amet ullamcorper est tempus id. Vivamus iaculis, eros vel accumsan congue, diam nisi elementum ex, non condimentum nibh quam vel velit.'
    },
  ];

  @override
  Widget build(BuildContext context) =>
    Container(
      child: Column(
        children: [
          HeaderSection(title: 'ÚLTIMAS CLASES AÑADIDAS'),
          SizedBox(height: 10,),
          Padding(
            padding: EdgeInsets.all(16.0),
            child: LessonsListWidget(_staticNews),
          ),
        ],
      ),
    );
}
