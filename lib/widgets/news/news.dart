import 'package:flutter/material.dart';

import '../triangle.dart';
import 'news_widgets/info.dart';

class NewWidget extends StatelessWidget {
  final Map<String, dynamic> staticNew;

  const NewWidget(this.staticNew);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width * 0.85,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Text(
            staticNew['date'],
            textAlign: TextAlign.right,
            style: TextStyle(color: Colors.grey, fontSize: 16),
          ),
          SizedBox(height: 15,),
          Container(
            height: 100,
            child: Stack(
              children: [
                InfoWidget(staticNew),
                Align(
                  alignment: Alignment.bottomRight,
                  child: ClipPath(
                    clipper: CustomTriangleClipper(),
                    child: Container(
                      width: 30,
                      height: 30,
                      color: Colors.white
                    ),
                  ),
                )
              ]
            )
          ),
        ],
      ),
    );
  }
}
