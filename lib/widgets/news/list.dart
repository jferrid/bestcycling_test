import 'package:flutter/material.dart';
import 'news.dart';

class LessonsListWidget extends StatelessWidget {
  final List<Map<String, dynamic>> news;

  const LessonsListWidget(this.news);

  @override
  Widget build(BuildContext context) =>
    Container(
      height: 150,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: news.length,
        itemBuilder: (context, index) {
          Map<String, dynamic> _new = news[index];
          return Container(
            margin: EdgeInsets.only(right: 16),
            child: NewWidget(_new),
          );
        }
      ),
    );
}
