import 'package:flutter/material.dart';

class InfoWidget extends StatelessWidget {
  final Map<String, dynamic> staticNew;
  const InfoWidget(this.staticNew);

  @override
  Widget build(BuildContext context) =>
    Container(
      padding: EdgeInsets.all(16),
      color: Color(0xFF262626),
      child: Text(
        staticNew['resume'],
        overflow: TextOverflow.ellipsis,
        maxLines: 4,
        textAlign: TextAlign.left,
        style: TextStyle(
          color: Colors.white,
          fontSize: 15
        ),
      )
    );
}
