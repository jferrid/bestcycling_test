import 'package:flutter/material.dart';

class LevelWidget extends StatelessWidget {
  final int level;
  const LevelWidget(this.level);

  @override
  Widget build(BuildContext context) =>
    Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Text(
          'Nivel',
          style: TextStyle(color: Colors.white, fontSize: 14),
        ),
        SizedBox(width: 6,),
        Container(
          width: 100,
          height: 50,
          child: ListView.builder(
            physics: NeverScrollableScrollPhysics(),
            scrollDirection: Axis.horizontal,
            itemCount: 3,
            itemBuilder: (context, index) =>
              Container(
                padding: EdgeInsets.all(4),
                margin: EdgeInsets.only(right: 8),
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: index < level ? Colors.white : Colors.grey,
                ),
              ),
          ),
        )
      ],
    );
}
