import './trainer.dart';

class TrainingClass {
  final String category;
  final int categoryNr;
  final String cover;
  final String graph;
  final int delaySeconds;
  final int durationTraining;
  final String graphWatts;
  final String id;
  final String level;
  final int levelNr;
  final int lifeFlexibilityPoints;
  final int lifeForcePoints;
  final int lifeMindPoints;
  final int lifeResistancePoints;
  final String shortTitle;
  final String subtitle;
  final String title;
  final String content;
  final String trainingType;
  final int trainingTypeNr;
  final int updatedAtTimestamp;
  final String relationships;
  final Trainer trainer;
  final String publishedAt;

  TrainingClass({
    this.category,
    this.categoryNr,
    this.cover,
    this.graph,
    this.delaySeconds,
    this.durationTraining,
    this.graphWatts,
    this.id,
    this.level,
    this.levelNr,
    this.lifeFlexibilityPoints,
    this.lifeForcePoints,
    this.lifeMindPoints,
    this.lifeResistancePoints,
    this.shortTitle,
    this.subtitle,
    this.title,
    this.content,
    this.trainingType,
    this.trainingTypeNr,
    this.updatedAtTimestamp,
    this.relationships,
    this.trainer,
    this.publishedAt,
  });

  TrainingClass getFromJSON(Map data) => TrainingClass(
    category: data['training_type'],
    levelNr: data['level_nr'],
    title: data['title'],
    durationTraining: data['duration_training'],
    cover: data['cover'],
    graph: data['graph'],
    trainingTypeNr: data['training_type_nr'],
    publishedAt: data['published_at']
  );
}