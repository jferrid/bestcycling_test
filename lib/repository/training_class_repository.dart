import 'dart:convert';
import 'package:bestcycling_test/models/training_class.dart';
import 'package:http/http.dart';

class TrainingClassRepository {
  static TrainingClassRepository _instance = TrainingClassRepository();
  static TrainingClassRepository get instance => _instance;

  List<TrainingClass> trainingClasses;

  Future<void> getTrainingClasses() async {
    String urlClient = 'https://apiv2.bestcycling.es/api/v2/training_classes?&include=trainer';
    final Map<String,String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer f5fbfeb375d513be8e3addcdbe7334a4744f5141fb57dcb54c863a8bad57cbc7',
    };

    var request = await get(
      urlClient,
      headers: headers,
    );

    Map<String, dynamic> result = json.decode(request.body);
    trainingClasses = result['data'].map((item) => TrainingClass().getFromJSON(item['attributes'])).toList().cast<TrainingClass>();
  }
}